removeoldimages:
	docker rmi fleximage
removeolddeployment:
	docker stop flexapi; docker rm flexapi;
buildimage:
	docker build -t fleximage .;
deployimage:
	docker run -d --name flexapi -p 5000:80 fleximage
redeployimage:
	make removeolddeployment; make removeoldimages; make buildimage; make deployimage;
connecttodeployment:
	docker exec -it flexapi bash;
