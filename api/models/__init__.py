"""
All Model Objects should be imported here to simplify imports from other modules
"""
from .book import Book