class Book:
    def __init__(self, title, author, description=None):
        self.title = title
        self.description = description
        self.author = author

    @property
    def serialize(self):
        result = {}
        if self.title:
            result["title"] = self.title
        if self.author:
            result["author"] = self.author
        if self.description:
            result["description"] = self.description
        return result