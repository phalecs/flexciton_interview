import os
import json
import unittest
from main import app
from flask import url_for
 
class MainTests(unittest.TestCase):
 
    ############################
    #### setup and teardown ####
    ############################
 
    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.assertEqual(app.debug, False)
 
    # executed after each test
    def tearDown(self):
        pass
 
    def test_invalid_url(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 404)
    
    def test_retrievebook_with_invalid_userid_0(self):
        userid = 0
        response = self.app.get("users/{}/books/favourite".format(userid), follow_redirects=True)
        self.assertEqual(response.status_code, 404)
 
    def test_retrievebook_with_valid_userid_1(self):
        userid = 1
        response = self.app.get("users/{}/books/favourite".format(userid), follow_redirects=True)
        assert response.status_code == 200
        response = json.loads(response.data)
        assert "title" in response
        assert "author" in response

    def test_retrievebook_with_valid_userid_2(self):
        userid = 2
        response = self.app.get("users/{}/books/favourite".format(userid), follow_redirects=True)
        assert response.status_code == 200
        response = json.loads(response.data)
        assert "title" in response
        assert "author" in response

if __name__ == "__main__":
    unittest.main()