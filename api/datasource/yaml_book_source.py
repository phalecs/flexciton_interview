import yaml
from .book_source import BookSource
from models.book import Book
import os

YAML_PATH = 'data/favourite-book.yml'
class YamlBookSource(BookSource):
    def __init__(self, path=YAML_PATH):
        if not os.path.isfile(os.path.join(os.getcwd(),YAML_PATH)):
            raise ValueError("Invalid File Path")
        self.source_path = YAML_PATH

    def __retrieve__(self):
        with open(self.source_path, 'r') as file:
            book = yaml.load(file)
            return book
        
    def __parse__(self, response):
        return Book(response.get("title"), response.get("authors"), response.get("description"))