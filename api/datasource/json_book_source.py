import requests
from .book_source import BookSource
from models.book import Book

API_PATH = "http://openlibrary.org/api/books?bibkeys=ISBN:0201558025&jscmd=data&format=json"

class JsonBookSource(BookSource):
    def __init__(self, path=API_PATH):
        self.source_path = API_PATH

    def __retrieve__(self):
        response = requests.get(self.source_path).json()
        return response
    
    def __parse__(self, response):
        for key in response:
            response = response.get(key)
            title = response.get("title")
            description = response.get("description")
            authors = []
            for item in response.get("authors"):
                name = item.get("name")
                if name:
                    authors.append(name)
            return Book(title, authors, description)