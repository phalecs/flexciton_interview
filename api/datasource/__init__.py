"""
All sources should be Imported Here to Simplify Imports from Other Modules
"""
from .json_book_source import JsonBookSource
from .yaml_book_source import YamlBookSource

def retrievedatasource(userid):
    """ Returns an instance of the appropraite datasource based on the User Id
        Keyword arguments:
        userid -- this is used for deciding the appropraite datasource
    """
    if userid == 1:
        return YamlBookSource()
    elif userid == 2:
        return JsonBookSource()
    else:
        raise ValueError("Datasource Not Implemented")
