from abc import ABCMeta, abstractmethod
class BookSource(metaclass= ABCMeta):
    @abstractmethod
    def __init__(self, path):
        pass

    @abstractmethod
    def __retrieve__(self):
        """ Retrieves the values from current datasource
        """
        pass
        
    @abstractmethod
    def __parse__(self, retrieved_response):
        """ parses the retrieved response of the datasource to a book object.
        """
        pass

    def retrievebookfromsource(self):
        return self.__parse__(self.__retrieve__())