import sys
sys.path.append("../")
import yaml
import requests
import logging
from models import Book
from datasource import JsonBookSource, YamlBookSource, retrievedatasource
from flask import Flask, jsonify


def create_app(debug = False):
    vapp = Flask(__name__)
    vapp.debug = debug
    return vapp


app = create_app()

@app.route("/users/<int:userid>/books/favourite")
def retrievebook(userid):
    try:
        datasource = retrievedatasource(userid)
    except ValueError as exception:
        logging.exception(exception)
        datasource = None
    if datasource:
        book = datasource.retrievebookfromsource()
        if book:
            return jsonify(book.serialize), 200, {'ContentType':'application/json'}
    return "Not Found", 404, {'ContentType':'application/text'}

if __name__ == "__main__":
    app.threaded = True
    app.run()