#Running the application without Docker

### (optional if host default is Python 3.6) Create a virtual environment with python dependency of version 3.6. potentially to isolate dependencies. An alternative to conda is venv which comes bundled with python
`conda create -n flexcciton python=3.6`

### (optional if host default is Python 3.6) Activate virtual environment
`activate flexciton`

### Install dependencies
`pip install -r requirements.txt`

### Change directory into the API folder and Run application
`cd api`
`python main.py` api can be found on port 5000 of the localhost

### Run tests
`pytest -v`


#Deploying Application with Docker

`make redeployimage` api can be found on port 5000 of the host system

#Running tests in Container
connect to container
`make connecttodeployment`
run the command below to run the tests
`pytest -v`
exit from the container
`exit`